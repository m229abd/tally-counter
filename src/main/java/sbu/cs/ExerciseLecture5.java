package sbu.cs;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    Random rand = new Random();
    public String weakPassword(int length) {
        StringBuilder pass = new StringBuilder();
        for (int i =0; i < length; i++)
            pass.append((char)('a' + rand.nextInt(26)));
        return pass.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        StringBuilder pass = new StringBuilder();
        if(length > 2){
            for(int i = 0; i < length; i++)
                pass.append((char)('!' + rand.nextInt(90)));
        }
        else
            throw new Exception();
        Pattern patt = Pattern.compile("^(?=.*[0-9])(?=.*[A-Z])(?=.*[!\"#$%&'()*+,\\-./:;<=>\\?@\\[\\]\\\\^_`]).*$");
        Matcher m = patt.matcher(pass);
        if(m.find())
            return pass.toString();
        return strongPassword(length);
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        ExerciseLecture4 fibo = new ExerciseLecture4();
        int i = 0;
        while(i <= n){
            int count = 0;
            long fib = fibo.fibonacci(i);
            String bin = Integer.toBinaryString((int)(fib));
            for(int j = 0; j < bin.length(); j++)
                if(bin.charAt(j) == '1')
                    count++;
            if((fib + count) == n)
                return true;
            i++;
        }
        return false;
    }
}
